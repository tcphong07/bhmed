

// Js Menu mobile header
$(".menu-mobile .ti-menu").click(function () {
  $('.header').addClass('show-menu');
});
$(".close-menu").click(function () {
  $('.header').removeClass('show-menu');
});

//show search
$(".btn-search-mb").click(function(){
  $('.top-head').addClass('show-search-mb');

});
$(".search-head .close-search").click(function(){
    $('.top-head').removeClass('show-search-mb');
    
});
// Sticky head menu to srcoll
$(window).scroll(function () {
  let contentHeadHeigh = $(".content-head").height();
  var sticky = $('header'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});
// slider home
// $('.slider-home').owlCarousel({
//     loop:true,
//     margin:30,
//     nav:true,
//     responsive:{
//         0:{
//             items:1
//         },
//         600:{
//             items:1
//         },
//         1000:{
//             items:1
//         }
//     }
// })

// Elements Slider  homepage
$(document).ready(function () {
  $('.slider-home').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// Elements san pham 
$(document).ready(function () {
  $('.list-serviess').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// Elements san pham goi y 
$(document).ready(function () {
  $('.slider-dichvukhac').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// Elements chung chi quoc te
$(document).ready(function () {
  $('.chungchi-quocte').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 2,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 5,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 5,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// Elements khach hang
$(document).ready(function () {
  $('.list-customers').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// Elements Slider show full images page detail
$(document).ready(function () {
  $('.slider-show-full-img').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    // autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// Elements Slider show full images page detail
$(document).ready(function () {
  $('.slider-show-full-img-certificate').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    // autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

/* ----------Js select box -----------------*/

/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            $('.select-selected').addClass('active');
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
      
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

/* ----------ENd Js select box -----------------*/


$(".menu-cate-mb").click(function(){
  $(".menu-sidebar-page").toggleClass("active");
});

$(".menu-close-mb").click(function(){
  $(".menu-sidebar-page").toggleClass("active");
});

function toggleIcon(e) {
  $(e.target)
    .prev('.panel-heading')
    .find(".more-less")
    .toggleClass('ti-plus ti-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);


const overlayTrigger = document.querySelector('.overlay-launch'),
  overlay = document.querySelector('.overlay'),
  tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";

const firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
let player,
  source;



function onYouTubeIframeAPIReady(evt) {
  console.log(source);
  player = new YT.Player('player', {
    height: '380px',
    width: '640'
  });
}


function launchOverlay(evt) {
  const $trigger = evt.target;
  source = $trigger.dataset.src;
  overlay.classList.toggle('_active');
  player.loadVideoById(source, 5, "large")


  if (!overlay.classList.contains('_active')) {
    player.stopVideo();
  } else {
    player.playVideo();
  }
}

overlayTrigger.addEventListener('click', launchOverlay);
